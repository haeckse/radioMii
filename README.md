# radioMii

**Mii** 美音

美 _(beautiful)_ 音 _(sound)_

radioMii is a web application to browse and play internet radio streams.

The app pulls the information about the radio streams from the [radio-browser.info](https://www.radio-browser.info/#/) server.

Try it at [radiomii.com](https://radiomii.com)

## Electron desktop app

### Run radioMii wrapped in an Electron app on a development server

npm run electron-dev

### Build a radioMii Electron application

npm run electron-package

### Translations

The UI has been translated into 30 languages. Thank you [mondstern](https://translate.codeberg.org/user/mondstern/) for the great work!

### License

[MIT](https://opensource.org/licenses/mit-license.php)
